﻿
#include <iostream>

#include <iostream>
#include <stack> 

using namespace std;

int main() {
    
    setlocale (LC_ALL, "rus");

    stack <int> steck; 

    int i = 0;
    
    int x;
    
    cin >> x;

    int* p = &x;

    cout << "Введите " << *p << " любых целых чисел : " << endl; 
    while (i != *p) 
    {
        int a;
        cin >> a;

        steck.push(a); 
        i++;
    }

    if (steck.empty()) cout << "Стек не пуст"; 

    cout << "Верхний элемент стека: " << steck.top() << endl; 
    cout << "Давайте удалим верхний элемент " << endl;

    steck.pop(); 

    cout << "Новый верхний элемент: " << steck.top(); 
}